
class Player

  def play_turn(warrior)
    
    if @comienzo == nil
      @health =warrior.health 
      @comienzo = 1
      @rescate = 0  
    end
    if warrior.health < 20  && warrior.feel.empty? &&  warrior.health >= @health   
        warrior.rest!
    elsif warrior.feel.empty? &&  warrior.health < @health && warrior.health < 10
      warrior.walk!(:backward)
    elsif @rescate == 1
      reversa(warrior)
    else
      sentir(warrior)
    end
    @health = warrior.health
  end
  
  def sentir(warrior)
    case
    when warrior.feel.stairs?
      if warrior.feel.enemy?
        warrior.attack!
      elsif warrior.feel.empty?
        if @rescate == 0
          @rescate = 1
          reversa(warrior)
        else
          warrior.walk!
        end
      else
        warrior.walk!
      end
    when warrior.feel.enemy?
      warrior.attack!
    when warrior.feel.captive?
      warrior.rescue!
    when warrior.feel.empty?
      warrior.walk!
    end
  end
  
  def reversa(warrior)
    if warrior.feel.empty?
      warrior.walk!(:backward)
    elsif warrior.feel(:backward).captive?
      warrior.rescue!(:backward)
      @rescate = 2
    end
  end
end
  